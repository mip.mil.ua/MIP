=== README.txt ===

This directory contains XML instance documents for the JC3IEDM WS/OO XSD.

Due to the significant amount of maintenance, the MIP XML WP decided to limit
the number of examples. Presently, there are three examples that correspond to
sections 9.6, 16.7.6, and 17.7.4 of the JC3IEDM main document.

Example 9.6 has been created with greatest care and comes as a referentially
complete XML document and as a sequence of XML documents with incremental
updates. The other two examples are valid with regard to the WS/OO XSD but they
may have semantic errors (e.g., violations of business rules).

The examples given in the following sections may also be considered in the future:

 - Section 7.3.7
 - Section 7.4.5
 - Section 14.2.4/14.2.5
 - Section 16.9.2.5
 - Section 18.6