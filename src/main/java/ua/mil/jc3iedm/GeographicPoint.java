
package ua.mil.jc3iedm;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * An ABSOLUTE-POINT that has its position specified with respect to the surface of the World Geodetic System 1984 (WGS 84) ellipsoid.
 * 
 * <p>Java class for GeographicPoint complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GeographicPoint">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}AbstractAbsolutePoint">
 *       &lt;sequence>
 *         &lt;element name="LatitudeCoordinate" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}LatitudeCoordinateTypeRangeLatitude9_6"/>
 *         &lt;element name="LongitudeCoordinate" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}LongitudeCoordinateTypeRangeLongitude10_6"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GeographicPoint", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", propOrder = {
    "latitudeCoordinate",
    "longitudeCoordinate"
})
public class GeographicPoint
    extends AbstractAbsolutePoint
{

    @XmlElement(name = "LatitudeCoordinate", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", required = true)
    protected BigDecimal latitudeCoordinate;
    @XmlElement(name = "LongitudeCoordinate", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", required = true)
    protected BigDecimal longitudeCoordinate;

    /**
     * Gets the value of the latitudeCoordinate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLatitudeCoordinate() {
        return latitudeCoordinate;
    }

    /**
     * Sets the value of the latitudeCoordinate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLatitudeCoordinate(BigDecimal value) {
        this.latitudeCoordinate = value;
    }

    /**
     * Gets the value of the longitudeCoordinate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLongitudeCoordinate() {
        return longitudeCoordinate;
    }

    /**
     * Sets the value of the longitudeCoordinate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLongitudeCoordinate(BigDecimal value) {
        this.longitudeCoordinate = value;
    }

}
