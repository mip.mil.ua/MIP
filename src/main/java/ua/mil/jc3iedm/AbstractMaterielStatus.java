
package ua.mil.jc3iedm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * An OBJECT-ITEM-STATUS that is a record of condition of a specific MATERIEL.
 * 
 * <p>Java class for AbstractMaterielStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AbstractMaterielStatus">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}AbstractObjectItemStatus">
 *       &lt;sequence>
 *         &lt;element name="OperationalStatusCode" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}MaterielStatusOperationalStatusCode"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AbstractMaterielStatus", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", propOrder = {
    "operationalStatusCode"
})
@XmlSeeAlso({
    OtherMaterielStatus.class
})
public abstract class AbstractMaterielStatus
    extends AbstractObjectItemStatus
{

    @XmlElement(name = "OperationalStatusCode", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", required = true)
    @XmlSchemaType(name = "token")
    protected MaterielStatusOperationalStatusCode operationalStatusCode;

    /**
     * Gets the value of the operationalStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link MaterielStatusOperationalStatusCode }
     *     
     */
    public MaterielStatusOperationalStatusCode getOperationalStatusCode() {
        return operationalStatusCode;
    }

    /**
     * Sets the value of the operationalStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link MaterielStatusOperationalStatusCode }
     *     
     */
    public void setOperationalStatusCode(MaterielStatusOperationalStatusCode value) {
        this.operationalStatusCode = value;
    }

}
