
package ua.mil.jc3iedm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * An individually identified class of objects that has military or civilian significance.
 * 
 * <p>Java class for AbstractObjectType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AbstractObjectType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OID" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}OIDType"/>
 *         &lt;element name="CreatorId" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}OIDType"/>
 *         &lt;element name="DecoyIndicatorCode" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}ObjectTypeDecoyIndicatorCode"/>
 *         &lt;element name="NameText" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}TextTypeVar100"/>
 *       &lt;/sequence>
 *       &lt;attribute name="typeCategory" type="{http://www.w3.org/2001/XMLSchema}string" fixed="Entity" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AbstractObjectType", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", propOrder = {
    "oid",
    "creatorId",
    "decoyIndicatorCode",
    "nameText"
})
@XmlSeeAlso({
    AbstractOrganisationType.class,
    AbstractMaterielType.class
})
public abstract class AbstractObjectType {

    @XmlElement(name = "OID", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String oid;
    @XmlElement(name = "CreatorId", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String creatorId;
    @XmlElement(name = "DecoyIndicatorCode", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", required = true)
    @XmlSchemaType(name = "token")
    protected ObjectTypeDecoyIndicatorCode decoyIndicatorCode;
    @XmlElement(name = "NameText", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", required = true)
    protected String nameText;
    @XmlAttribute(name = "typeCategory")
    protected String typeCategory;

    /**
     * Gets the value of the oid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOID() {
        return oid;
    }

    /**
     * Sets the value of the oid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOID(String value) {
        this.oid = value;
    }

    /**
     * Gets the value of the creatorId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreatorId() {
        return creatorId;
    }

    /**
     * Sets the value of the creatorId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreatorId(String value) {
        this.creatorId = value;
    }

    /**
     * Gets the value of the decoyIndicatorCode property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectTypeDecoyIndicatorCode }
     *     
     */
    public ObjectTypeDecoyIndicatorCode getDecoyIndicatorCode() {
        return decoyIndicatorCode;
    }

    /**
     * Sets the value of the decoyIndicatorCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectTypeDecoyIndicatorCode }
     *     
     */
    public void setDecoyIndicatorCode(ObjectTypeDecoyIndicatorCode value) {
        this.decoyIndicatorCode = value;
    }

    /**
     * Gets the value of the nameText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameText() {
        return nameText;
    }

    /**
     * Sets the value of the nameText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameText(String value) {
        this.nameText = value;
    }

    /**
     * Gets the value of the typeCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeCategory() {
        if (typeCategory == null) {
            return "Entity";
        } else {
            return typeCategory;
        }
    }

    /**
     * Sets the value of the typeCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeCategory(String value) {
        this.typeCategory = value;
    }

}
