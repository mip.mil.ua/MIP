
package ua.mil.jc3iedm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * A reference to some AbsolutePoint - A POINT in a geodetic system.
 * 
 * <p>Java class for AbstractAbsolutePointRef complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AbstractAbsolutePointRef">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}AbstractPointRef">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AbstractAbsolutePointRef", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2")
@XmlSeeAlso({
    GeographicPointRef.class
})
public class AbstractAbsolutePointRef
    extends AbstractPointRef
{


}
