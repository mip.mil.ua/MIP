
package ua.mil.jc3iedm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * A reference to some EquipmentType - A MATERIEL-TYPE that is not intended for consumption.
 * 
 * <p>Java class for AbstractEquipmentTypeRef complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AbstractEquipmentTypeRef">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}AbstractMaterielTypeRef">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AbstractEquipmentTypeRef", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2")
@XmlSeeAlso({
    VehicleTypeRef.class,
    WeaponTypeRef.class
})
public class AbstractEquipmentTypeRef
    extends AbstractMaterielTypeRef
{


}
