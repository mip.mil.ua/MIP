
package ua.mil.jc3iedm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * An OBJECT-TYPE that represents equipment, apparatus or supplies of military interest without distinction to its application for administrative or combat purposes.
 * 
 * <p>Java class for AbstractMaterielType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AbstractMaterielType">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}AbstractObjectType">
 *       &lt;sequence>
 *         &lt;element name="ReportableItemText" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}TextTypeVar6" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AbstractMaterielType", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", propOrder = {
    "reportableItemText"
})
@XmlSeeAlso({
    AbstractEquipmentType.class
})
public abstract class AbstractMaterielType
    extends AbstractObjectType
{

    @XmlElement(name = "ReportableItemText", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2")
    protected String reportableItemText;

    /**
     * Gets the value of the reportableItemText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportableItemText() {
        return reportableItemText;
    }

    /**
     * Sets the value of the reportableItemText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportableItemText(String value) {
        this.reportableItemText = value;
    }

}
