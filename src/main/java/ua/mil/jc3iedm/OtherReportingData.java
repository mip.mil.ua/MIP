
package ua.mil.jc3iedm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * The specification of source, quality and timing that applies to reported data.
 * 
 * <p>Java class for OtherReportingData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OtherReportingData">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}AbstractReportingData">
 *       &lt;sequence>
 *         &lt;element name="TimingCategoryCode" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}ReportingDataTimingCategoryCode"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OtherReportingData", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", propOrder = {
    "timingCategoryCode"
})
public class OtherReportingData
    extends AbstractReportingData
{

    @XmlElement(name = "TimingCategoryCode", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", required = true)
    @XmlSchemaType(name = "token")
    protected ReportingDataTimingCategoryCode timingCategoryCode;

    /**
     * Gets the value of the timingCategoryCode property.
     * 
     * @return
     *     possible object is
     *     {@link ReportingDataTimingCategoryCode }
     *     
     */
    public ReportingDataTimingCategoryCode getTimingCategoryCode() {
        return timingCategoryCode;
    }

    /**
     * Sets the value of the timingCategoryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReportingDataTimingCategoryCode }
     *     
     */
    public void setTimingCategoryCode(ReportingDataTimingCategoryCode value) {
        this.timingCategoryCode = value;
    }

}
