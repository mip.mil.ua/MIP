
package ua.mil.jc3iedm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded">
 *         &lt;element ref="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}GeographicPoint"/>
 *         &lt;element ref="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}ObjectItemComment"/>
 *         &lt;element ref="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}ObjectItemHostilityStatus"/>
 *         &lt;element ref="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}OtherMateriel"/>
 *         &lt;element ref="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}OtherMaterielRef"/>
 *         &lt;element ref="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}OtherMaterielStatus"/>
 *         &lt;element ref="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}OtherOrganisation"/>
 *         &lt;element ref="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}OtherOrganisationType"/>
 *         &lt;element ref="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}OtherReportingData"/>
 *         &lt;element ref="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}VehicleType"/>
 *         &lt;element ref="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}VehicleTypeRef"/>
 *         &lt;element ref="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}WeaponType"/>
 *         &lt;element ref="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}WeaponTypeRef"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "geographicPointOrObjectItemCommentOrObjectItemHostilityStatus"
})
@XmlRootElement(name = "JC3IEDM", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2")
public class JC3IEDM {

    @XmlElements({
        @XmlElement(name = "GeographicPoint", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", type = GeographicPoint.class),
        @XmlElement(name = "ObjectItemComment", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", type = ObjectItemComment.class),
        @XmlElement(name = "ObjectItemHostilityStatus", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", type = ObjectItemHostilityStatus.class),
        @XmlElement(name = "OtherMateriel", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", type = OtherMateriel.class),
        @XmlElement(name = "OtherMaterielRef", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", type = OtherMaterielRef.class),
        @XmlElement(name = "OtherMaterielStatus", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", type = OtherMaterielStatus.class),
        @XmlElement(name = "OtherOrganisation", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", type = OtherOrganisation.class),
        @XmlElement(name = "OtherOrganisationType", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", type = OtherOrganisationType.class),
        @XmlElement(name = "OtherReportingData", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", type = OtherReportingData.class),
        @XmlElement(name = "VehicleType", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", type = VehicleType.class),
        @XmlElement(name = "VehicleTypeRef", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", type = VehicleTypeRef.class),
        @XmlElement(name = "WeaponType", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", type = WeaponType.class),
        @XmlElement(name = "WeaponTypeRef", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", type = WeaponTypeRef.class)
    })
    protected List<Object> geographicPointOrObjectItemCommentOrObjectItemHostilityStatus;

    /**
     * Gets the value of the geographicPointOrObjectItemCommentOrObjectItemHostilityStatus property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the geographicPointOrObjectItemCommentOrObjectItemHostilityStatus property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGeographicPointOrObjectItemCommentOrObjectItemHostilityStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GeographicPoint }
     * {@link ObjectItemComment }
     * {@link ObjectItemHostilityStatus }
     * {@link OtherMateriel }
     * {@link OtherMaterielRef }
     * {@link OtherMaterielStatus }
     * {@link OtherOrganisation }
     * {@link OtherOrganisationType }
     * {@link OtherReportingData }
     * {@link VehicleType }
     * {@link VehicleTypeRef }
     * {@link WeaponType }
     * {@link WeaponTypeRef }
     * 
     * 
     */
    public List<Object> getGeographicPointOrObjectItemCommentOrObjectItemHostilityStatus() {
        if (geographicPointOrObjectItemCommentOrObjectItemHostilityStatus == null) {
            geographicPointOrObjectItemCommentOrObjectItemHostilityStatus = new ArrayList<Object>();
        }
        return this.geographicPointOrObjectItemCommentOrObjectItemHostilityStatus;
    }

}
