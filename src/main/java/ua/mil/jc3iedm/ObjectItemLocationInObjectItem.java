
package ua.mil.jc3iedm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A ObjectItem/Location association. Location is the child in a 'is-geometrically-defined-through' relationship; ObjectItemLocation provides additional information on the association.
 * 
 * <p>Java class for ObjectItemLocationInObjectItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ObjectItemLocationInObjectItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="Location" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}AbstractLocation"/>
 *           &lt;element name="LocationRef" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}AbstractLocationRef"/>
 *         &lt;/choice>
 *         &lt;choice>
 *           &lt;element name="ObjectItemLocation" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}ObjectItemLocation"/>
 *           &lt;element name="ObjectItemLocationRef" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}ObjectItemLocationRef"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *       &lt;attribute name="typeCategory" type="{http://www.w3.org/2001/XMLSchema}string" fixed="Association" />
 *       &lt;attribute name="associationRole" type="{http://www.w3.org/2001/XMLSchema}string" fixed="" />
 *       &lt;attribute name="associationStart" type="{http://www.w3.org/2001/XMLSchema}string" fixed="ObjectItem" />
 *       &lt;attribute name="associationEnd" type="{http://www.w3.org/2001/XMLSchema}string" fixed="Location" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectItemLocationInObjectItem", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", propOrder = {
    "location",
    "locationRef",
    "objectItemLocation",
    "objectItemLocationRef"
})
public class ObjectItemLocationInObjectItem {

    @XmlElement(name = "Location", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2")
    protected AbstractLocation location;
    @XmlElement(name = "LocationRef", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2")
    protected AbstractLocationRef locationRef;
    @XmlElement(name = "ObjectItemLocation", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2")
    protected ObjectItemLocation objectItemLocation;
    @XmlElement(name = "ObjectItemLocationRef", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2")
    protected ObjectItemLocationRef objectItemLocationRef;
    @XmlAttribute(name = "typeCategory")
    protected String typeCategory;
    @XmlAttribute(name = "associationRole")
    protected String associationRole;
    @XmlAttribute(name = "associationStart")
    protected String associationStart;
    @XmlAttribute(name = "associationEnd")
    protected String associationEnd;

    /**
     * Gets the value of the location property.
     * 
     * @return
     *     possible object is
     *     {@link AbstractLocation }
     *     
     */
    public AbstractLocation getLocation() {
        return location;
    }

    /**
     * Sets the value of the location property.
     * 
     * @param value
     *     allowed object is
     *     {@link AbstractLocation }
     *     
     */
    public void setLocation(AbstractLocation value) {
        this.location = value;
    }

    /**
     * Gets the value of the locationRef property.
     * 
     * @return
     *     possible object is
     *     {@link AbstractLocationRef }
     *     
     */
    public AbstractLocationRef getLocationRef() {
        return locationRef;
    }

    /**
     * Sets the value of the locationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link AbstractLocationRef }
     *     
     */
    public void setLocationRef(AbstractLocationRef value) {
        this.locationRef = value;
    }

    /**
     * Gets the value of the objectItemLocation property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectItemLocation }
     *     
     */
    public ObjectItemLocation getObjectItemLocation() {
        return objectItemLocation;
    }

    /**
     * Sets the value of the objectItemLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectItemLocation }
     *     
     */
    public void setObjectItemLocation(ObjectItemLocation value) {
        this.objectItemLocation = value;
    }

    /**
     * Gets the value of the objectItemLocationRef property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectItemLocationRef }
     *     
     */
    public ObjectItemLocationRef getObjectItemLocationRef() {
        return objectItemLocationRef;
    }

    /**
     * Sets the value of the objectItemLocationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectItemLocationRef }
     *     
     */
    public void setObjectItemLocationRef(ObjectItemLocationRef value) {
        this.objectItemLocationRef = value;
    }

    /**
     * Gets the value of the typeCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeCategory() {
        if (typeCategory == null) {
            return "Association";
        } else {
            return typeCategory;
        }
    }

    /**
     * Sets the value of the typeCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeCategory(String value) {
        this.typeCategory = value;
    }

    /**
     * Gets the value of the associationRole property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssociationRole() {
        if (associationRole == null) {
            return "";
        } else {
            return associationRole;
        }
    }

    /**
     * Sets the value of the associationRole property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssociationRole(String value) {
        this.associationRole = value;
    }

    /**
     * Gets the value of the associationStart property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssociationStart() {
        if (associationStart == null) {
            return "ObjectItem";
        } else {
            return associationStart;
        }
    }

    /**
     * Sets the value of the associationStart property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssociationStart(String value) {
        this.associationStart = value;
    }

    /**
     * Gets the value of the associationEnd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssociationEnd() {
        if (associationEnd == null) {
            return "Location";
        } else {
            return associationEnd;
        }
    }

    /**
     * Sets the value of the associationEnd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssociationEnd(String value) {
        this.associationEnd = value;
    }

}
