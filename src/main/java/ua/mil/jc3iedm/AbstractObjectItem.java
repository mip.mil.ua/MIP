
package ua.mil.jc3iedm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * An individually identified object that has military or civilian significance.
 * 
 * <p>Java class for AbstractObjectItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AbstractObjectItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OID" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}OIDType"/>
 *         &lt;element name="CreatorId" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}OIDType"/>
 *         &lt;element name="NameText" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}TextTypeVar100"/>
 *         &lt;choice maxOccurs="unbounded" minOccurs="0">
 *           &lt;element name="Comment" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}ObjectItemComment"/>
 *           &lt;element name="CommentRef" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}ObjectItemCommentRef"/>
 *         &lt;/choice>
 *         &lt;choice maxOccurs="unbounded" minOccurs="0">
 *           &lt;element name="HostilityStatus" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}ObjectItemHostilityStatus"/>
 *           &lt;element name="HostilityStatusRef" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}ObjectItemHostilityStatusRef"/>
 *         &lt;/choice>
 *         &lt;element name="ObjectItemLocationInObjectItem" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}ObjectItemLocationInObjectItem" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ObjectItemTypeInObjectItem" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}ObjectItemTypeInObjectItem" maxOccurs="unbounded"/>
 *         &lt;choice maxOccurs="unbounded" minOccurs="0">
 *           &lt;element name="Status" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}AbstractObjectItemStatus"/>
 *           &lt;element name="StatusRef" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}AbstractObjectItemStatusRef"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *       &lt;attribute name="typeCategory" type="{http://www.w3.org/2001/XMLSchema}string" fixed="Entity" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AbstractObjectItem", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", propOrder = {
    "oid",
    "creatorId",
    "nameText",
    "commentOrCommentRef",
    "hostilityStatusOrHostilityStatusRef",
    "objectItemLocationInObjectItem",
    "objectItemTypeInObjectItem",
    "statusOrStatusRef"
})
@XmlSeeAlso({
    AbstractOrganisation.class,
    AbstractMateriel.class
})
public abstract class AbstractObjectItem {

    @XmlElement(name = "OID", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String oid;
    @XmlElement(name = "CreatorId", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String creatorId;
    @XmlElement(name = "NameText", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", required = true)
    protected String nameText;
    @XmlElements({
        @XmlElement(name = "Comment", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", type = ObjectItemComment.class),
        @XmlElement(name = "CommentRef", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", type = ObjectItemCommentRef.class)
    })
    protected List<Object> commentOrCommentRef;
    @XmlElements({
        @XmlElement(name = "HostilityStatus", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", type = ObjectItemHostilityStatus.class),
        @XmlElement(name = "HostilityStatusRef", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", type = ObjectItemHostilityStatusRef.class)
    })
    protected List<Object> hostilityStatusOrHostilityStatusRef;
    @XmlElement(name = "ObjectItemLocationInObjectItem", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2")
    protected List<ObjectItemLocationInObjectItem> objectItemLocationInObjectItem;
    @XmlElement(name = "ObjectItemTypeInObjectItem", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", required = true)
    protected List<ObjectItemTypeInObjectItem> objectItemTypeInObjectItem;
    @XmlElements({
        @XmlElement(name = "Status", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", type = AbstractObjectItemStatus.class),
        @XmlElement(name = "StatusRef", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", type = AbstractObjectItemStatusRef.class)
    })
    protected List<Object> statusOrStatusRef;
    @XmlAttribute(name = "typeCategory")
    protected String typeCategory;

    /**
     * Gets the value of the oid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOID() {
        return oid;
    }

    /**
     * Sets the value of the oid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOID(String value) {
        this.oid = value;
    }

    /**
     * Gets the value of the creatorId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreatorId() {
        return creatorId;
    }

    /**
     * Sets the value of the creatorId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreatorId(String value) {
        this.creatorId = value;
    }

    /**
     * Gets the value of the nameText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameText() {
        return nameText;
    }

    /**
     * Sets the value of the nameText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameText(String value) {
        this.nameText = value;
    }

    /**
     * Gets the value of the commentOrCommentRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the commentOrCommentRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCommentOrCommentRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectItemComment }
     * {@link ObjectItemCommentRef }
     * 
     * 
     */
    public List<Object> getCommentOrCommentRef() {
        if (commentOrCommentRef == null) {
            commentOrCommentRef = new ArrayList<Object>();
        }
        return this.commentOrCommentRef;
    }

    /**
     * Gets the value of the hostilityStatusOrHostilityStatusRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the hostilityStatusOrHostilityStatusRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHostilityStatusOrHostilityStatusRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectItemHostilityStatus }
     * {@link ObjectItemHostilityStatusRef }
     * 
     * 
     */
    public List<Object> getHostilityStatusOrHostilityStatusRef() {
        if (hostilityStatusOrHostilityStatusRef == null) {
            hostilityStatusOrHostilityStatusRef = new ArrayList<Object>();
        }
        return this.hostilityStatusOrHostilityStatusRef;
    }

    /**
     * Gets the value of the objectItemLocationInObjectItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the objectItemLocationInObjectItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getObjectItemLocationInObjectItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectItemLocationInObjectItem }
     * 
     * 
     */
    public List<ObjectItemLocationInObjectItem> getObjectItemLocationInObjectItem() {
        if (objectItemLocationInObjectItem == null) {
            objectItemLocationInObjectItem = new ArrayList<ObjectItemLocationInObjectItem>();
        }
        return this.objectItemLocationInObjectItem;
    }

    /**
     * Gets the value of the objectItemTypeInObjectItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the objectItemTypeInObjectItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getObjectItemTypeInObjectItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectItemTypeInObjectItem }
     * 
     * 
     */
    public List<ObjectItemTypeInObjectItem> getObjectItemTypeInObjectItem() {
        if (objectItemTypeInObjectItem == null) {
            objectItemTypeInObjectItem = new ArrayList<ObjectItemTypeInObjectItem>();
        }
        return this.objectItemTypeInObjectItem;
    }

    /**
     * Gets the value of the statusOrStatusRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the statusOrStatusRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatusOrStatusRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AbstractObjectItemStatus }
     * {@link AbstractObjectItemStatusRef }
     * 
     * 
     */
    public List<Object> getStatusOrStatusRef() {
        if (statusOrStatusRef == null) {
            statusOrStatusRef = new ArrayList<Object>();
        }
        return this.statusOrStatusRef;
    }

    /**
     * Gets the value of the typeCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeCategory() {
        if (typeCategory == null) {
            return "Entity";
        } else {
            return typeCategory;
        }
    }

    /**
     * Sets the value of the typeCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeCategory(String value) {
        this.typeCategory = value;
    }

}
