
package ua.mil.jc3iedm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * A comment that adds information about a specific OBJECT-ITEM.
 * 
 * <p>Java class for ObjectItemComment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ObjectItemComment">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OID" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}OIDType"/>
 *         &lt;element name="CreatorId" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}OIDType"/>
 *         &lt;element name="SymbolAnnotationText" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}TextTypeVar20" minOccurs="0"/>
 *         &lt;choice>
 *           &lt;element name="ReportingData" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}AbstractReportingData"/>
 *           &lt;element name="ReportingDataRef" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}AbstractReportingDataRef"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *       &lt;attribute name="typeCategory" type="{http://www.w3.org/2001/XMLSchema}string" fixed="Entity" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectItemComment", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", propOrder = {
    "oid",
    "creatorId",
    "symbolAnnotationText",
    "reportingData",
    "reportingDataRef"
})
public class ObjectItemComment {

    @XmlElement(name = "OID", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String oid;
    @XmlElement(name = "CreatorId", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String creatorId;
    @XmlElement(name = "SymbolAnnotationText", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2")
    protected String symbolAnnotationText;
    @XmlElement(name = "ReportingData", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2")
    protected AbstractReportingData reportingData;
    @XmlElement(name = "ReportingDataRef", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2")
    protected AbstractReportingDataRef reportingDataRef;
    @XmlAttribute(name = "typeCategory")
    protected String typeCategory;

    /**
     * Gets the value of the oid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOID() {
        return oid;
    }

    /**
     * Sets the value of the oid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOID(String value) {
        this.oid = value;
    }

    /**
     * Gets the value of the creatorId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreatorId() {
        return creatorId;
    }

    /**
     * Sets the value of the creatorId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreatorId(String value) {
        this.creatorId = value;
    }

    /**
     * Gets the value of the symbolAnnotationText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSymbolAnnotationText() {
        return symbolAnnotationText;
    }

    /**
     * Sets the value of the symbolAnnotationText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSymbolAnnotationText(String value) {
        this.symbolAnnotationText = value;
    }

    /**
     * Gets the value of the reportingData property.
     * 
     * @return
     *     possible object is
     *     {@link AbstractReportingData }
     *     
     */
    public AbstractReportingData getReportingData() {
        return reportingData;
    }

    /**
     * Sets the value of the reportingData property.
     * 
     * @param value
     *     allowed object is
     *     {@link AbstractReportingData }
     *     
     */
    public void setReportingData(AbstractReportingData value) {
        this.reportingData = value;
    }

    /**
     * Gets the value of the reportingDataRef property.
     * 
     * @return
     *     possible object is
     *     {@link AbstractReportingDataRef }
     *     
     */
    public AbstractReportingDataRef getReportingDataRef() {
        return reportingDataRef;
    }

    /**
     * Sets the value of the reportingDataRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link AbstractReportingDataRef }
     *     
     */
    public void setReportingDataRef(AbstractReportingDataRef value) {
        this.reportingDataRef = value;
    }

    /**
     * Gets the value of the typeCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeCategory() {
        if (typeCategory == null) {
            return "Entity";
        } else {
            return typeCategory;
        }
    }

    /**
     * Sets the value of the typeCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeCategory(String value) {
        this.typeCategory = value;
    }

}
