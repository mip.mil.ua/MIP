
package ua.mil.jc3iedm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * A POINT in a geodetic system.
 * 
 * <p>Java class for AbstractAbsolutePoint complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AbstractAbsolutePoint">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}AbstractPoint">
 *       &lt;sequence>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="VerticalDistance" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}VerticalDistance"/>
 *           &lt;element name="VerticalDistanceRef" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}VerticalDistanceRef"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AbstractAbsolutePoint", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", propOrder = {
    "verticalDistance",
    "verticalDistanceRef"
})
@XmlSeeAlso({
    GeographicPoint.class
})
public abstract class AbstractAbsolutePoint
    extends AbstractPoint
{

    @XmlElement(name = "VerticalDistance", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2")
    protected VerticalDistance verticalDistance;
    @XmlElement(name = "VerticalDistanceRef", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2")
    protected VerticalDistanceRef verticalDistanceRef;

    /**
     * Gets the value of the verticalDistance property.
     * 
     * @return
     *     possible object is
     *     {@link VerticalDistance }
     *     
     */
    public VerticalDistance getVerticalDistance() {
        return verticalDistance;
    }

    /**
     * Sets the value of the verticalDistance property.
     * 
     * @param value
     *     allowed object is
     *     {@link VerticalDistance }
     *     
     */
    public void setVerticalDistance(VerticalDistance value) {
        this.verticalDistance = value;
    }

    /**
     * Gets the value of the verticalDistanceRef property.
     * 
     * @return
     *     possible object is
     *     {@link VerticalDistanceRef }
     *     
     */
    public VerticalDistanceRef getVerticalDistanceRef() {
        return verticalDistanceRef;
    }

    /**
     * Sets the value of the verticalDistanceRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link VerticalDistanceRef }
     *     
     */
    public void setVerticalDistanceRef(VerticalDistanceRef value) {
        this.verticalDistanceRef = value;
    }

}
