
package ua.mil.jc3iedm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * A reference to some WeaponType - An EQUIPMENT-TYPE of any kind used in warfare or combat to attack and overcome an enemy.
 * 
 * <p>Java class for WeaponTypeRef complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WeaponTypeRef">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}AbstractEquipmentTypeRef">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WeaponTypeRef", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2")
public class WeaponTypeRef
    extends AbstractEquipmentTypeRef
{


}
