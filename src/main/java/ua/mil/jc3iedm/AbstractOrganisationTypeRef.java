
package ua.mil.jc3iedm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * A reference to some OrganisationType - An OBJECT-TYPE that represents administrative or functional structures.
 * 
 * <p>Java class for AbstractOrganisationTypeRef complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AbstractOrganisationTypeRef">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}AbstractObjectTypeRef">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AbstractOrganisationTypeRef", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2")
public class AbstractOrganisationTypeRef
    extends AbstractObjectTypeRef
{


}
