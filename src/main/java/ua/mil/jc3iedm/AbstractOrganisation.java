
package ua.mil.jc3iedm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * An OBJECT-ITEM that is an administrative or functional structure.
 * 
 * <p>Java class for AbstractOrganisation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AbstractOrganisation">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}AbstractObjectItem">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AbstractOrganisation", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2")
@XmlSeeAlso({
    OtherOrganisation.class
})
public abstract class AbstractOrganisation
    extends AbstractObjectItem
{


}
