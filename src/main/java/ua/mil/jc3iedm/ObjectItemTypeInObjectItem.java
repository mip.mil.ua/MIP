
package ua.mil.jc3iedm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A ObjectItem/ObjectType association. ObjectType is the child in a 'is-classified-as' relationship; ObjectItemType provides additional information on the association.
 * 
 * <p>Java class for ObjectItemTypeInObjectItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ObjectItemTypeInObjectItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="ObjectType" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}AbstractObjectType"/>
 *           &lt;element name="ObjectTypeRef" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}AbstractObjectTypeRef"/>
 *         &lt;/choice>
 *         &lt;choice>
 *           &lt;element name="ObjectItemType" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}ObjectItemType"/>
 *           &lt;element name="ObjectItemTypeRef" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}ObjectItemTypeRef"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *       &lt;attribute name="typeCategory" type="{http://www.w3.org/2001/XMLSchema}string" fixed="Association" />
 *       &lt;attribute name="associationRole" type="{http://www.w3.org/2001/XMLSchema}string" fixed="" />
 *       &lt;attribute name="associationStart" type="{http://www.w3.org/2001/XMLSchema}string" fixed="ObjectItem" />
 *       &lt;attribute name="associationEnd" type="{http://www.w3.org/2001/XMLSchema}string" fixed="ObjectType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectItemTypeInObjectItem", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", propOrder = {
    "objectType",
    "objectTypeRef",
    "objectItemType",
    "objectItemTypeRef"
})
public class ObjectItemTypeInObjectItem {

    @XmlElement(name = "ObjectType", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2")
    protected AbstractObjectType objectType;
    @XmlElement(name = "ObjectTypeRef", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2")
    protected AbstractObjectTypeRef objectTypeRef;
    @XmlElement(name = "ObjectItemType", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2")
    protected ObjectItemType objectItemType;
    @XmlElement(name = "ObjectItemTypeRef", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2")
    protected ObjectItemTypeRef objectItemTypeRef;
    @XmlAttribute(name = "typeCategory")
    protected String typeCategory;
    @XmlAttribute(name = "associationRole")
    protected String associationRole;
    @XmlAttribute(name = "associationStart")
    protected String associationStart;
    @XmlAttribute(name = "associationEnd")
    protected String associationEnd;

    /**
     * Gets the value of the objectType property.
     * 
     * @return
     *     possible object is
     *     {@link AbstractObjectType }
     *     
     */
    public AbstractObjectType getObjectType() {
        return objectType;
    }

    /**
     * Sets the value of the objectType property.
     * 
     * @param value
     *     allowed object is
     *     {@link AbstractObjectType }
     *     
     */
    public void setObjectType(AbstractObjectType value) {
        this.objectType = value;
    }

    /**
     * Gets the value of the objectTypeRef property.
     * 
     * @return
     *     possible object is
     *     {@link AbstractObjectTypeRef }
     *     
     */
    public AbstractObjectTypeRef getObjectTypeRef() {
        return objectTypeRef;
    }

    /**
     * Sets the value of the objectTypeRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link AbstractObjectTypeRef }
     *     
     */
    public void setObjectTypeRef(AbstractObjectTypeRef value) {
        this.objectTypeRef = value;
    }

    /**
     * Gets the value of the objectItemType property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectItemType }
     *     
     */
    public ObjectItemType getObjectItemType() {
        return objectItemType;
    }

    /**
     * Sets the value of the objectItemType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectItemType }
     *     
     */
    public void setObjectItemType(ObjectItemType value) {
        this.objectItemType = value;
    }

    /**
     * Gets the value of the objectItemTypeRef property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectItemTypeRef }
     *     
     */
    public ObjectItemTypeRef getObjectItemTypeRef() {
        return objectItemTypeRef;
    }

    /**
     * Sets the value of the objectItemTypeRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectItemTypeRef }
     *     
     */
    public void setObjectItemTypeRef(ObjectItemTypeRef value) {
        this.objectItemTypeRef = value;
    }

    /**
     * Gets the value of the typeCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeCategory() {
        if (typeCategory == null) {
            return "Association";
        } else {
            return typeCategory;
        }
    }

    /**
     * Sets the value of the typeCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeCategory(String value) {
        this.typeCategory = value;
    }

    /**
     * Gets the value of the associationRole property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssociationRole() {
        if (associationRole == null) {
            return "";
        } else {
            return associationRole;
        }
    }

    /**
     * Sets the value of the associationRole property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssociationRole(String value) {
        this.associationRole = value;
    }

    /**
     * Gets the value of the associationStart property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssociationStart() {
        if (associationStart == null) {
            return "ObjectItem";
        } else {
            return associationStart;
        }
    }

    /**
     * Sets the value of the associationStart property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssociationStart(String value) {
        this.associationStart = value;
    }

    /**
     * Gets the value of the associationEnd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssociationEnd() {
        if (associationEnd == null) {
            return "ObjectType";
        } else {
            return associationEnd;
        }
    }

    /**
     * Sets the value of the associationEnd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssociationEnd(String value) {
        this.associationEnd = value;
    }

}
