
package ua.mil.jc3iedm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * An OBJECT-ITEM-STATUS that is a record of condition of a specific MATERIEL.
 * 
 * <p>Java class for OtherMaterielStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OtherMaterielStatus">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}AbstractMaterielStatus">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OtherMaterielStatus", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2")
public class OtherMaterielStatus
    extends AbstractMaterielStatus
{


}
