
package ua.mil.jc3iedm;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * A specification of the altitude or height of a point or a level as measured with respect to a specified reference datum in the direction normal to the plane that is tangent to the WGS84 ellipsoid of revolution.
 * 
 * <p>Java class for VerticalDistance complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VerticalDistance">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OID" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}OIDType"/>
 *         &lt;element name="CreatorId" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}OIDType"/>
 *         &lt;element name="ReferenceCode" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}VerticalDistanceReferenceCode"/>
 *         &lt;element name="Dimension" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}DimensionType12_3"/>
 *         &lt;element name="DatumText" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}TextTypeVar50" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="typeCategory" type="{http://www.w3.org/2001/XMLSchema}string" fixed="Entity" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VerticalDistance", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", propOrder = {
    "oid",
    "creatorId",
    "referenceCode",
    "dimension",
    "datumText"
})
public class VerticalDistance {

    @XmlElement(name = "OID", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String oid;
    @XmlElement(name = "CreatorId", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String creatorId;
    @XmlElement(name = "ReferenceCode", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", required = true)
    @XmlSchemaType(name = "token")
    protected VerticalDistanceReferenceCode referenceCode;
    @XmlElement(name = "Dimension", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", required = true)
    protected BigDecimal dimension;
    @XmlElement(name = "DatumText", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2")
    protected String datumText;
    @XmlAttribute(name = "typeCategory")
    protected String typeCategory;

    /**
     * Gets the value of the oid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOID() {
        return oid;
    }

    /**
     * Sets the value of the oid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOID(String value) {
        this.oid = value;
    }

    /**
     * Gets the value of the creatorId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreatorId() {
        return creatorId;
    }

    /**
     * Sets the value of the creatorId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreatorId(String value) {
        this.creatorId = value;
    }

    /**
     * Gets the value of the referenceCode property.
     * 
     * @return
     *     possible object is
     *     {@link VerticalDistanceReferenceCode }
     *     
     */
    public VerticalDistanceReferenceCode getReferenceCode() {
        return referenceCode;
    }

    /**
     * Sets the value of the referenceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link VerticalDistanceReferenceCode }
     *     
     */
    public void setReferenceCode(VerticalDistanceReferenceCode value) {
        this.referenceCode = value;
    }

    /**
     * Gets the value of the dimension property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDimension() {
        return dimension;
    }

    /**
     * Sets the value of the dimension property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDimension(BigDecimal value) {
        this.dimension = value;
    }

    /**
     * Gets the value of the datumText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDatumText() {
        return datumText;
    }

    /**
     * Sets the value of the datumText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDatumText(String value) {
        this.datumText = value;
    }

    /**
     * Gets the value of the typeCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeCategory() {
        if (typeCategory == null) {
            return "Entity";
        } else {
            return typeCategory;
        }
    }

    /**
     * Sets the value of the typeCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeCategory(String value) {
        this.typeCategory = value;
    }

}
