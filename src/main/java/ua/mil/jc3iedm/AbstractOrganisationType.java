
package ua.mil.jc3iedm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * An OBJECT-TYPE that represents administrative or functional structures.
 * 
 * <p>Java class for AbstractOrganisationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AbstractOrganisationType">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}AbstractObjectType">
 *       &lt;sequence>
 *         &lt;element name="DescriptionText" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}TextTypeVar50" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AbstractOrganisationType", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", propOrder = {
    "descriptionText"
})
@XmlSeeAlso({
    OtherOrganisationType.class
})
public abstract class AbstractOrganisationType
    extends AbstractObjectType
{

    @XmlElement(name = "DescriptionText", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2")
    protected String descriptionText;

    /**
     * Gets the value of the descriptionText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescriptionText() {
        return descriptionText;
    }

    /**
     * Sets the value of the descriptionText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescriptionText(String value) {
        this.descriptionText = value;
    }

}
