
package ua.mil.jc3iedm;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ua.mil.jc3iedm package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ObjectItemHostilityStatus_QNAME = new QName("urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", "ObjectItemHostilityStatus");
    private final static QName _OtherMaterielStatus_QNAME = new QName("urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", "OtherMaterielStatus");
    private final static QName _OtherReportingData_QNAME = new QName("urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", "OtherReportingData");
    private final static QName _WeaponType_QNAME = new QName("urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", "WeaponType");
    private final static QName _OtherOrganisationType_QNAME = new QName("urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", "OtherOrganisationType");
    private final static QName _OtherOrganisation_QNAME = new QName("urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", "OtherOrganisation");
    private final static QName _VehicleTypeRef_QNAME = new QName("urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", "VehicleTypeRef");
    private final static QName _ObjectItemComment_QNAME = new QName("urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", "ObjectItemComment");
    private final static QName _WeaponTypeRef_QNAME = new QName("urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", "WeaponTypeRef");
    private final static QName _GeographicPoint_QNAME = new QName("urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", "GeographicPoint");
    private final static QName _OtherMaterielRef_QNAME = new QName("urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", "OtherMaterielRef");
    private final static QName _VehicleType_QNAME = new QName("urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", "VehicleType");
    private final static QName _OtherMateriel_QNAME = new QName("urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", "OtherMateriel");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ua.mil.jc3iedm
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OtherMaterielRef }
     * 
     */
    public OtherMaterielRef createOtherMaterielRef() {
        return new OtherMaterielRef();
    }

    /**
     * Create an instance of {@link OtherMateriel }
     * 
     */
    public OtherMateriel createOtherMateriel() {
        return new OtherMateriel();
    }

    /**
     * Create an instance of {@link VehicleType }
     * 
     */
    public VehicleType createVehicleType() {
        return new VehicleType();
    }

    /**
     * Create an instance of {@link ObjectItemComment }
     * 
     */
    public ObjectItemComment createObjectItemComment() {
        return new ObjectItemComment();
    }

    /**
     * Create an instance of {@link VehicleTypeRef }
     * 
     */
    public VehicleTypeRef createVehicleTypeRef() {
        return new VehicleTypeRef();
    }

    /**
     * Create an instance of {@link GeographicPoint }
     * 
     */
    public GeographicPoint createGeographicPoint() {
        return new GeographicPoint();
    }

    /**
     * Create an instance of {@link JC3IEDM }
     * 
     */
    public JC3IEDM createJC3IEDM() {
        return new JC3IEDM();
    }

    /**
     * Create an instance of {@link ObjectItemHostilityStatus }
     * 
     */
    public ObjectItemHostilityStatus createObjectItemHostilityStatus() {
        return new ObjectItemHostilityStatus();
    }

    /**
     * Create an instance of {@link OtherMaterielStatus }
     * 
     */
    public OtherMaterielStatus createOtherMaterielStatus() {
        return new OtherMaterielStatus();
    }

    /**
     * Create an instance of {@link OtherOrganisation }
     * 
     */
    public OtherOrganisation createOtherOrganisation() {
        return new OtherOrganisation();
    }

    /**
     * Create an instance of {@link OtherOrganisationType }
     * 
     */
    public OtherOrganisationType createOtherOrganisationType() {
        return new OtherOrganisationType();
    }

    /**
     * Create an instance of {@link OtherReportingData }
     * 
     */
    public OtherReportingData createOtherReportingData() {
        return new OtherReportingData();
    }

    /**
     * Create an instance of {@link WeaponType }
     * 
     */
    public WeaponType createWeaponType() {
        return new WeaponType();
    }

    /**
     * Create an instance of {@link WeaponTypeRef }
     * 
     */
    public WeaponTypeRef createWeaponTypeRef() {
        return new WeaponTypeRef();
    }

    /**
     * Create an instance of {@link AbstractEquipmentTypeRef }
     * 
     */
    public AbstractEquipmentTypeRef createAbstractEquipmentTypeRef() {
        return new AbstractEquipmentTypeRef();
    }

    /**
     * Create an instance of {@link ObjectItemTypeRef }
     * 
     */
    public ObjectItemTypeRef createObjectItemTypeRef() {
        return new ObjectItemTypeRef();
    }

    /**
     * Create an instance of {@link AbstractMaterielRef }
     * 
     */
    public AbstractMaterielRef createAbstractMaterielRef() {
        return new AbstractMaterielRef();
    }

    /**
     * Create an instance of {@link ObjectItemTypeInObjectItem }
     * 
     */
    public ObjectItemTypeInObjectItem createObjectItemTypeInObjectItem() {
        return new ObjectItemTypeInObjectItem();
    }

    /**
     * Create an instance of {@link AbstractObjectTypeRef }
     * 
     */
    public AbstractObjectTypeRef createAbstractObjectTypeRef() {
        return new AbstractObjectTypeRef();
    }

    /**
     * Create an instance of {@link AbstractLocationRef }
     * 
     */
    public AbstractLocationRef createAbstractLocationRef() {
        return new AbstractLocationRef();
    }

    /**
     * Create an instance of {@link AbstractOrganisationRef }
     * 
     */
    public AbstractOrganisationRef createAbstractOrganisationRef() {
        return new AbstractOrganisationRef();
    }

    /**
     * Create an instance of {@link AbstractMaterielStatusRef }
     * 
     */
    public AbstractMaterielStatusRef createAbstractMaterielStatusRef() {
        return new AbstractMaterielStatusRef();
    }

    /**
     * Create an instance of {@link ObjectItemLocation }
     * 
     */
    public ObjectItemLocation createObjectItemLocation() {
        return new ObjectItemLocation();
    }

    /**
     * Create an instance of {@link VerticalDistance }
     * 
     */
    public VerticalDistance createVerticalDistance() {
        return new VerticalDistance();
    }

    /**
     * Create an instance of {@link OtherOrganisationRef }
     * 
     */
    public OtherOrganisationRef createOtherOrganisationRef() {
        return new OtherOrganisationRef();
    }

    /**
     * Create an instance of {@link AbstractAbsolutePointRef }
     * 
     */
    public AbstractAbsolutePointRef createAbstractAbsolutePointRef() {
        return new AbstractAbsolutePointRef();
    }

    /**
     * Create an instance of {@link OtherMaterielStatusRef }
     * 
     */
    public OtherMaterielStatusRef createOtherMaterielStatusRef() {
        return new OtherMaterielStatusRef();
    }

    /**
     * Create an instance of {@link ObjectItemCommentRef }
     * 
     */
    public ObjectItemCommentRef createObjectItemCommentRef() {
        return new ObjectItemCommentRef();
    }

    /**
     * Create an instance of {@link ObjectItemHostilityStatusRef }
     * 
     */
    public ObjectItemHostilityStatusRef createObjectItemHostilityStatusRef() {
        return new ObjectItemHostilityStatusRef();
    }

    /**
     * Create an instance of {@link AbstractOrganisationTypeRef }
     * 
     */
    public AbstractOrganisationTypeRef createAbstractOrganisationTypeRef() {
        return new AbstractOrganisationTypeRef();
    }

    /**
     * Create an instance of {@link AbstractPointRef }
     * 
     */
    public AbstractPointRef createAbstractPointRef() {
        return new AbstractPointRef();
    }

    /**
     * Create an instance of {@link AbstractMaterielTypeRef }
     * 
     */
    public AbstractMaterielTypeRef createAbstractMaterielTypeRef() {
        return new AbstractMaterielTypeRef();
    }

    /**
     * Create an instance of {@link AbstractObjectItemStatusRef }
     * 
     */
    public AbstractObjectItemStatusRef createAbstractObjectItemStatusRef() {
        return new AbstractObjectItemStatusRef();
    }

    /**
     * Create an instance of {@link AbstractObjectItemRef }
     * 
     */
    public AbstractObjectItemRef createAbstractObjectItemRef() {
        return new AbstractObjectItemRef();
    }

    /**
     * Create an instance of {@link ObjectItemType }
     * 
     */
    public ObjectItemType createObjectItemType() {
        return new ObjectItemType();
    }

    /**
     * Create an instance of {@link OtherReportingDataRef }
     * 
     */
    public OtherReportingDataRef createOtherReportingDataRef() {
        return new OtherReportingDataRef();
    }

    /**
     * Create an instance of {@link ObjectItemLocationInObjectItem }
     * 
     */
    public ObjectItemLocationInObjectItem createObjectItemLocationInObjectItem() {
        return new ObjectItemLocationInObjectItem();
    }

    /**
     * Create an instance of {@link AbstractReportingDataRef }
     * 
     */
    public AbstractReportingDataRef createAbstractReportingDataRef() {
        return new AbstractReportingDataRef();
    }

    /**
     * Create an instance of {@link GeographicPointRef }
     * 
     */
    public GeographicPointRef createGeographicPointRef() {
        return new GeographicPointRef();
    }

    /**
     * Create an instance of {@link ObjectItemLocationRef }
     * 
     */
    public ObjectItemLocationRef createObjectItemLocationRef() {
        return new ObjectItemLocationRef();
    }

    /**
     * Create an instance of {@link VerticalDistanceRef }
     * 
     */
    public VerticalDistanceRef createVerticalDistanceRef() {
        return new VerticalDistanceRef();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectItemHostilityStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", name = "ObjectItemHostilityStatus")
    public JAXBElement<ObjectItemHostilityStatus> createObjectItemHostilityStatus(ObjectItemHostilityStatus value) {
        return new JAXBElement<ObjectItemHostilityStatus>(_ObjectItemHostilityStatus_QNAME, ObjectItemHostilityStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OtherMaterielStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", name = "OtherMaterielStatus")
    public JAXBElement<OtherMaterielStatus> createOtherMaterielStatus(OtherMaterielStatus value) {
        return new JAXBElement<OtherMaterielStatus>(_OtherMaterielStatus_QNAME, OtherMaterielStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OtherReportingData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", name = "OtherReportingData")
    public JAXBElement<OtherReportingData> createOtherReportingData(OtherReportingData value) {
        return new JAXBElement<OtherReportingData>(_OtherReportingData_QNAME, OtherReportingData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WeaponType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", name = "WeaponType")
    public JAXBElement<WeaponType> createWeaponType(WeaponType value) {
        return new JAXBElement<WeaponType>(_WeaponType_QNAME, WeaponType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OtherOrganisationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", name = "OtherOrganisationType")
    public JAXBElement<OtherOrganisationType> createOtherOrganisationType(OtherOrganisationType value) {
        return new JAXBElement<OtherOrganisationType>(_OtherOrganisationType_QNAME, OtherOrganisationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OtherOrganisation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", name = "OtherOrganisation")
    public JAXBElement<OtherOrganisation> createOtherOrganisation(OtherOrganisation value) {
        return new JAXBElement<OtherOrganisation>(_OtherOrganisation_QNAME, OtherOrganisation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VehicleTypeRef }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", name = "VehicleTypeRef")
    public JAXBElement<VehicleTypeRef> createVehicleTypeRef(VehicleTypeRef value) {
        return new JAXBElement<VehicleTypeRef>(_VehicleTypeRef_QNAME, VehicleTypeRef.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjectItemComment }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", name = "ObjectItemComment")
    public JAXBElement<ObjectItemComment> createObjectItemComment(ObjectItemComment value) {
        return new JAXBElement<ObjectItemComment>(_ObjectItemComment_QNAME, ObjectItemComment.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WeaponTypeRef }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", name = "WeaponTypeRef")
    public JAXBElement<WeaponTypeRef> createWeaponTypeRef(WeaponTypeRef value) {
        return new JAXBElement<WeaponTypeRef>(_WeaponTypeRef_QNAME, WeaponTypeRef.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GeographicPoint }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", name = "GeographicPoint")
    public JAXBElement<GeographicPoint> createGeographicPoint(GeographicPoint value) {
        return new JAXBElement<GeographicPoint>(_GeographicPoint_QNAME, GeographicPoint.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OtherMaterielRef }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", name = "OtherMaterielRef")
    public JAXBElement<OtherMaterielRef> createOtherMaterielRef(OtherMaterielRef value) {
        return new JAXBElement<OtherMaterielRef>(_OtherMaterielRef_QNAME, OtherMaterielRef.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VehicleType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", name = "VehicleType")
    public JAXBElement<VehicleType> createVehicleType(VehicleType value) {
        return new JAXBElement<VehicleType>(_VehicleType_QNAME, VehicleType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OtherMateriel }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", name = "OtherMateriel")
    public JAXBElement<OtherMateriel> createOtherMateriel(OtherMateriel value) {
        return new JAXBElement<OtherMateriel>(_OtherMateriel_QNAME, OtherMateriel.class, null, value);
    }

}
