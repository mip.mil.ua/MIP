
package ua.mil.jc3iedm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * An EQUIPMENT-TYPE of any kind used in warfare or combat to attack and overcome an enemy.
 * 
 * <p>Java class for WeaponType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WeaponType">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}AbstractEquipmentType">
 *       &lt;sequence>
 *         &lt;element name="CategoryCode" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}WeaponTypeCategoryCode"/>
 *         &lt;element name="SubcategoryCode" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}WeaponTypeSubcategoryCode" minOccurs="0"/>
 *         &lt;element name="CalibreText" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}TextTypeVar15" minOccurs="0"/>
 *         &lt;element name="FireGuidanceIndicatorCode" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}WeaponTypeFireGuidanceIndicatorCode" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WeaponType", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", propOrder = {
    "categoryCode",
    "subcategoryCode",
    "calibreText",
    "fireGuidanceIndicatorCode"
})
public class WeaponType
    extends AbstractEquipmentType
{

    @XmlElement(name = "CategoryCode", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", required = true)
    @XmlSchemaType(name = "token")
    protected WeaponTypeCategoryCode categoryCode;
    @XmlElement(name = "SubcategoryCode", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2")
    @XmlSchemaType(name = "token")
    protected WeaponTypeSubcategoryCode subcategoryCode;
    @XmlElement(name = "CalibreText", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2")
    protected String calibreText;
    @XmlElement(name = "FireGuidanceIndicatorCode", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2")
    @XmlSchemaType(name = "token")
    protected WeaponTypeFireGuidanceIndicatorCode fireGuidanceIndicatorCode;

    /**
     * Gets the value of the categoryCode property.
     * 
     * @return
     *     possible object is
     *     {@link WeaponTypeCategoryCode }
     *     
     */
    public WeaponTypeCategoryCode getCategoryCode() {
        return categoryCode;
    }

    /**
     * Sets the value of the categoryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeaponTypeCategoryCode }
     *     
     */
    public void setCategoryCode(WeaponTypeCategoryCode value) {
        this.categoryCode = value;
    }

    /**
     * Gets the value of the subcategoryCode property.
     * 
     * @return
     *     possible object is
     *     {@link WeaponTypeSubcategoryCode }
     *     
     */
    public WeaponTypeSubcategoryCode getSubcategoryCode() {
        return subcategoryCode;
    }

    /**
     * Sets the value of the subcategoryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeaponTypeSubcategoryCode }
     *     
     */
    public void setSubcategoryCode(WeaponTypeSubcategoryCode value) {
        this.subcategoryCode = value;
    }

    /**
     * Gets the value of the calibreText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalibreText() {
        return calibreText;
    }

    /**
     * Sets the value of the calibreText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalibreText(String value) {
        this.calibreText = value;
    }

    /**
     * Gets the value of the fireGuidanceIndicatorCode property.
     * 
     * @return
     *     possible object is
     *     {@link WeaponTypeFireGuidanceIndicatorCode }
     *     
     */
    public WeaponTypeFireGuidanceIndicatorCode getFireGuidanceIndicatorCode() {
        return fireGuidanceIndicatorCode;
    }

    /**
     * Sets the value of the fireGuidanceIndicatorCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeaponTypeFireGuidanceIndicatorCode }
     *     
     */
    public void setFireGuidanceIndicatorCode(WeaponTypeFireGuidanceIndicatorCode value) {
        this.fireGuidanceIndicatorCode = value;
    }

}
