
package ua.mil.jc3iedm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * A reference to some ObjectItem - An individually identified object that has military or civilian significance.
 * 
 * <p>Java class for AbstractObjectItemRef complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AbstractObjectItemRef">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OID" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}OIDType"/>
 *         &lt;choice maxOccurs="unbounded" minOccurs="0">
 *           &lt;element name="Comment" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}ObjectItemComment"/>
 *           &lt;element name="CommentRef" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}ObjectItemCommentRef"/>
 *         &lt;/choice>
 *         &lt;choice maxOccurs="unbounded" minOccurs="0">
 *           &lt;element name="HostilityStatus" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}ObjectItemHostilityStatus"/>
 *           &lt;element name="HostilityStatusRef" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}ObjectItemHostilityStatusRef"/>
 *         &lt;/choice>
 *         &lt;element name="ObjectItemLocationInObjectItem" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}ObjectItemLocationInObjectItem" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ObjectItemTypeInObjectItem" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}ObjectItemTypeInObjectItem" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;choice maxOccurs="unbounded" minOccurs="0">
 *           &lt;element name="Status" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}AbstractObjectItemStatus"/>
 *           &lt;element name="StatusRef" type="{urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2}AbstractObjectItemStatusRef"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AbstractObjectItemRef", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", propOrder = {
    "oid",
    "commentOrCommentRef",
    "hostilityStatusOrHostilityStatusRef",
    "objectItemLocationInObjectItem",
    "objectItemTypeInObjectItem",
    "statusOrStatusRef"
})
@XmlSeeAlso({
    AbstractMaterielRef.class,
    AbstractOrganisationRef.class
})
public class AbstractObjectItemRef {

    @XmlElement(name = "OID", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String oid;
    @XmlElements({
        @XmlElement(name = "Comment", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", type = ObjectItemComment.class),
        @XmlElement(name = "CommentRef", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", type = ObjectItemCommentRef.class)
    })
    protected List<Object> commentOrCommentRef;
    @XmlElements({
        @XmlElement(name = "HostilityStatus", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", type = ObjectItemHostilityStatus.class),
        @XmlElement(name = "HostilityStatusRef", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", type = ObjectItemHostilityStatusRef.class)
    })
    protected List<Object> hostilityStatusOrHostilityStatusRef;
    @XmlElement(name = "ObjectItemLocationInObjectItem", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2")
    protected List<ObjectItemLocationInObjectItem> objectItemLocationInObjectItem;
    @XmlElement(name = "ObjectItemTypeInObjectItem", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2")
    protected List<ObjectItemTypeInObjectItem> objectItemTypeInObjectItem;
    @XmlElements({
        @XmlElement(name = "Status", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", type = AbstractObjectItemStatus.class),
        @XmlElement(name = "StatusRef", namespace = "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2", type = AbstractObjectItemStatusRef.class)
    })
    protected List<Object> statusOrStatusRef;

    /**
     * Gets the value of the oid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOID() {
        return oid;
    }

    /**
     * Sets the value of the oid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOID(String value) {
        this.oid = value;
    }

    /**
     * Gets the value of the commentOrCommentRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the commentOrCommentRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCommentOrCommentRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectItemComment }
     * {@link ObjectItemCommentRef }
     * 
     * 
     */
    public List<Object> getCommentOrCommentRef() {
        if (commentOrCommentRef == null) {
            commentOrCommentRef = new ArrayList<Object>();
        }
        return this.commentOrCommentRef;
    }

    /**
     * Gets the value of the hostilityStatusOrHostilityStatusRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the hostilityStatusOrHostilityStatusRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHostilityStatusOrHostilityStatusRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectItemHostilityStatus }
     * {@link ObjectItemHostilityStatusRef }
     * 
     * 
     */
    public List<Object> getHostilityStatusOrHostilityStatusRef() {
        if (hostilityStatusOrHostilityStatusRef == null) {
            hostilityStatusOrHostilityStatusRef = new ArrayList<Object>();
        }
        return this.hostilityStatusOrHostilityStatusRef;
    }

    /**
     * Gets the value of the objectItemLocationInObjectItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the objectItemLocationInObjectItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getObjectItemLocationInObjectItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectItemLocationInObjectItem }
     * 
     * 
     */
    public List<ObjectItemLocationInObjectItem> getObjectItemLocationInObjectItem() {
        if (objectItemLocationInObjectItem == null) {
            objectItemLocationInObjectItem = new ArrayList<ObjectItemLocationInObjectItem>();
        }
        return this.objectItemLocationInObjectItem;
    }

    /**
     * Gets the value of the objectItemTypeInObjectItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the objectItemTypeInObjectItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getObjectItemTypeInObjectItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectItemTypeInObjectItem }
     * 
     * 
     */
    public List<ObjectItemTypeInObjectItem> getObjectItemTypeInObjectItem() {
        if (objectItemTypeInObjectItem == null) {
            objectItemTypeInObjectItem = new ArrayList<ObjectItemTypeInObjectItem>();
        }
        return this.objectItemTypeInObjectItem;
    }

    /**
     * Gets the value of the statusOrStatusRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the statusOrStatusRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatusOrStatusRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AbstractObjectItemStatus }
     * {@link AbstractObjectItemStatusRef }
     * 
     * 
     */
    public List<Object> getStatusOrStatusRef() {
        if (statusOrStatusRef == null) {
            statusOrStatusRef = new ArrayList<Object>();
        }
        return this.statusOrStatusRef;
    }

}
