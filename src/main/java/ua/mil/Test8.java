package ua.mil;

import ua.mil.jc3iedm.*;

import java.util.Date;

/**
 * Created by alex on 08.12.15.
 */
public class Test8 {
    public static final String comment = "изменение объекта: локация (lat = 15.0, lon = 15.0, alt = 50)";

    public static JC3IEDM generate(AbstractOrganisationRef me, String oid) {
        // ReportingData

        MipHelper mipHelper = new MipHelper(me);

        OtherReportingData reportingData = mipHelper.getOtherReportingData(new Date());

         //-----------------

        OtherMaterielRef materielRef = createObject(mipHelper, reportingData, oid);

        JC3IEDM jc3IEDM = new JC3IEDM();
        jc3IEDM.getGeographicPointOrObjectItemCommentOrObjectItemHostilityStatus().add(materielRef);

        return jc3IEDM;
    }

    private static OtherMaterielRef createObject(MipHelper mipHelper, OtherReportingData reportingData, String oid) {

        OtherMaterielRef objectItemRef = new OtherMaterielRef();
        objectItemRef.setOID(oid);

        // Location
        final double lat = 15.0;
        final double lon = 15.0;
        final double alt = 50;
        final double bearing = 100;
        GeographicPoint locaton = mipHelper.createGeographicPoint(lat, lon, alt);
        GeographicPointRef geographicPointRef = new GeographicPointRef();
        geographicPointRef.setOID(locaton.getOID());

        // ObjectItemLocation
        ObjectItemLocationInObjectItem locationInObjectItem = mipHelper.createObjectItemLocation(bearing);
        locationInObjectItem.setLocation(locaton);
        locationInObjectItem.getObjectItemLocation().setReportingData(reportingData);
        objectItemRef.getObjectItemLocationInObjectItem().add(locationInObjectItem);
        //-----------------


        return objectItemRef;
    }

}
