package ua.mil;

import ua.mil.jc3iedm.*;

import java.util.Date;

/**
 * Created by alex on 08.12.15.
 */
public class Test2 {
    public static final String comment = "Вражеский ПТРК: lat = 15.0, lon = 15.0, alt = Nan, направление 0 (север), рабочий, с комментарием";

    private static OtherMateriel createObject(MipHelper mipHelper, OtherReportingData reportingData) {

        OtherReportingDataRef reportingDataRef = new OtherReportingDataRef();
        reportingDataRef.setOID(reportingData.getOID());

        final double lat = 15.0;
        final double lon = 15.0;
        final double alt = Double.NaN;
        final double bearing = 0;
        final String typeId = "6618a0b3-f473-4f67-843c-4d2030b66dfd";
        final MaterielStatusOperationalStatusCode status = MaterielStatusOperationalStatusCode.OPR;
        final ObjectItemHostilityStatusCode hostility = ObjectItemHostilityStatusCode.HO;
        final String commentText = "Описание ПТРК";
        final String objectName = "Вражеский ПТРК";

        // OtherMateriel
        OtherMateriel objectItem = mipHelper.createOtherMateriel(objectName);

        // ObjectItemLocation
        GeographicPoint geographicPoint = mipHelper.createGeographicPoint(lat, lon, alt);
        ObjectItemLocationInObjectItem locationInObjectItem = mipHelper.createObjectItemLocation(bearing);
        locationInObjectItem.setLocation(geographicPoint);
        locationInObjectItem.getObjectItemLocation().setReportingDataRef(reportingDataRef);
        objectItem.getObjectItemLocationInObjectItem().add(locationInObjectItem);
        //-----------------

        // ObjectItemType
        ObjectItemTypeInObjectItem itemTypeInObjectItem = mipHelper.createObjectItemTypeInObjectItemWeapon(mipHelper.createWeaponTypeRef(typeId));
        itemTypeInObjectItem.getObjectItemType().setReportingDataRef(reportingDataRef);
        objectItem.getObjectItemTypeInObjectItem().add(itemTypeInObjectItem);
        //-----------------
        // ObjectMaterialStatus
        OtherMaterielStatus materielStatus = mipHelper.createMaterielStatus(status);
        materielStatus.setReportingDataRef(reportingDataRef);
        objectItem.getStatusOrStatusRef().add(materielStatus);

        //Hostility
        ObjectItemHostilityStatus hostilityStatus = mipHelper.createObjectItemHostilityStatus(hostility);
        hostilityStatus.setReportingDataRef(reportingDataRef);
        objectItem.getHostilityStatusOrHostilityStatusRef().add(hostilityStatus);

        // Comment
        ObjectItemComment comment = mipHelper.createObjectItemComment(commentText);
        comment.setReportingDataRef(reportingDataRef);
        objectItem.getCommentOrCommentRef().add(comment);
        //-----------------
        return objectItem;
    }

    public static JC3IEDM generate(AbstractOrganisationRef me) {
        // ReportingData

        MipHelper mipHelper = new MipHelper(me);

        OtherReportingData reportingData = mipHelper.getOtherReportingData(new Date());
        //-----------------

        OtherMateriel objectItem = createObject(mipHelper, reportingData);
        JC3IEDM jc3IEDM = new JC3IEDM();
        jc3IEDM.getGeographicPointOrObjectItemCommentOrObjectItemHostilityStatus().add(objectItem);
        jc3IEDM.getGeographicPointOrObjectItemCommentOrObjectItemHostilityStatus().add(reportingData);

        return jc3IEDM;
    }
}
