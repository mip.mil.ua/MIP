package ua.mil;

import com.sun.xml.internal.txw2.output.IndentingXMLStreamWriter;
import org.xml.sax.SAXException;
import ua.mil.jc3iedm.AbstractOrganisationRef;
import ua.mil.jc3iedm.JC3IEDM;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.*;
import java.util.UUID;

public class Main {

    public static void main(String[] args) throws IOException, JAXBException, SAXException, XMLStreamException {
        // Me
        AbstractOrganisationRef me = new AbstractOrganisationRef();
        me.setOID("603");
        //-----------------

        writeXml(Test1.generate(me), "./schema/examples/object_item/example1.xml", Test1.comment);
        writeXml(Test2.generate(me), "./schema/examples/object_item/example2.xml", Test2.comment);
        writeXml(Test3.generate(me), "./schema/examples/object_item/example3.xml", Test3.comment);
        writeXml(Test4.generate(me), "./schema/examples/object_item/example4.xml", Test4.comment);
        writeXml(Test5.generate(me), "./schema/examples/object_item/example5.xml", Test5.comment);
        writeXml(Test6.generate(me), "./schema/examples/object_item/example6.xml", Test6.comment);
        writeXml(Test9.generate(me), "./schema/examples/object_item/example7.xml", Test9.comment);
        String guid = UUID.randomUUID().toString();
        writeXml(Test7.generate(me, guid), "./schema/examples/object_item/example8.xml", "{" + guid + "} " + Test7.comment);
        writeXml(Test8.generate(me, guid), "./schema/examples/object_item/example9.xml", "{" + guid + "} " + Test8.comment);
    }

    private static void writeXml(JC3IEDM jc3IEDM, String filename, String comment) throws SAXException, JAXBException, IOException, XMLStreamException {
        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = schemaFactory.newSchema(new File("schema/Root.xsd"));
        XMLOutputFactory xof = XMLOutputFactory.newFactory();
        try (OutputStream fos = new FileOutputStream(new File(filename))) {
            XMLStreamWriter xsw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(fos));
            xsw.writeStartDocument();
            xsw.writeComment(comment);
            xsw.writeCharacters("\r\n");

            JAXBContext jc = JAXBContext.newInstance(JC3IEDM.class);

            Marshaller marshaller = jc.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
            // Validate with schema
            marshaller.setSchema(schema);
            // Prettify
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            // Set schema location
            marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "urn:int:nato:standard:mip:jc3iedm:3.1.4:oo:2.2 ../../../schema/Root.xsd");

            // Marshall to XML
            marshaller.marshal(jc3IEDM, xsw);
        }
    }
}
