package ua.mil;

import ua.mil.jc3iedm.*;

import java.util.Date;

/**
 * Created by alex on 08.12.15.
 */
public class Test7 {
    public static final String comment = "БТР без параметров";

    public static JC3IEDM generate(AbstractOrganisationRef me, String oid) {
        // ReportingData

        MipHelper mipHelper = new MipHelper(me);

        OtherReportingData reportingData = mipHelper.getOtherReportingData(new Date());
        OtherReportingDataRef reportingDataRef = new OtherReportingDataRef();
        reportingDataRef.setOID(reportingData.getOID());

        OtherMateriel objectItem1 = createObject1(mipHelper, reportingData);
        objectItem1.setOID(oid);

        JC3IEDM jc3IEDM = new JC3IEDM();
        jc3IEDM.getGeographicPointOrObjectItemCommentOrObjectItemHostilityStatus().add(objectItem1);

        return jc3IEDM;
    }

    private static OtherMateriel createObject1(MipHelper mipHelper, OtherReportingData reportingData) {

        final String typeId = "69a8fe95-9977-46f7-a7de-6add479a177f";
        final String objectName = "БТР";

        // OtherMateriel
        OtherMateriel objectItem = mipHelper.createOtherMateriel(objectName);

        // ObjectItemType
        ObjectItemTypeInObjectItem itemTypeInObjectItem = mipHelper.createObjectItemTypeInObjectItemWeapon(mipHelper.createvehicleTypeRef(typeId));
        itemTypeInObjectItem.getObjectItemType().setReportingData(reportingData);
        objectItem.getObjectItemTypeInObjectItem().add(itemTypeInObjectItem);
        //-----------------

        return objectItem;
    }

}
