package ua.mil;

import ua.mil.jc3iedm.*;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * Created by alex on 08.12.15.
 */
public class MipHelper {
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("YYYYMMddhhmmss.sss");
    private final AbstractOrganisationRef me;

    public MipHelper(AbstractOrganisationRef me) {
        this.me = me;
    }

    public OtherReportingData getOtherReportingData(Date date) {
        OtherReportingData reportingData = new OtherReportingData();
        reportingData.setOID(generateUUID()); // Auto-generated
        reportingData.setCreatorId(me.getOID());
        reportingData.setReportingDatetime(String.valueOf(dateFormat.format(date)));
        reportingData.setReportingOrganisationRef(me);
        reportingData.setTimingCategoryCode(ReportingDataTimingCategoryCode.RDABST);
        return reportingData;
    }

    public  OtherMateriel createOtherMateriel(String objectName) {
        OtherMateriel objectItem = new OtherMateriel();
        objectItem.setOID(generateUUID());
        objectItem.setCreatorId(me.getOID());
        objectItem.setNameText(objectName);
        return objectItem;
    }

    public  ObjectItemComment createObjectItemComment(String commentText) {
        ObjectItemComment comment = new ObjectItemComment();
        comment.setOID(generateUUID());
        comment.setCreatorId(me.getOID());
        comment.setSymbolAnnotationText(commentText);
        return comment;
    }

    public  ObjectItemHostilityStatus createObjectItemHostilityStatus(ObjectItemHostilityStatusCode hostility) {
        ObjectItemHostilityStatus hostilityStatus = new ObjectItemHostilityStatus();
        hostilityStatus.setOID(generateUUID());
        hostilityStatus.setCreatorId(me.getOID());
        hostilityStatus.setCode(hostility);
        return hostilityStatus;
    }

    public  OtherMaterielStatus createMaterielStatus(MaterielStatusOperationalStatusCode status) {
        // Material Status
        OtherMaterielStatus materielStatus = new OtherMaterielStatus();
        materielStatus.setOID(generateUUID());
        materielStatus.setCreatorId(me.getOID());
        materielStatus.setOperationalStatusCode(status);
        return materielStatus;
    }

    public  ObjectItemTypeInObjectItem createObjectItemTypeInObjectItemWeapon(AbstractMaterielTypeRef weaponTypeRef) {
        ObjectItemType objectItemType = new ObjectItemType();
        objectItemType.setOID(generateUUID()); // Auto-generated
        objectItemType.setCreatorId(me.getOID());

        ObjectItemTypeInObjectItem itemTypeInObjectItem = new ObjectItemTypeInObjectItem();
        itemTypeInObjectItem.setObjectTypeRef(weaponTypeRef);
        itemTypeInObjectItem.setObjectItemType(objectItemType);
        return itemTypeInObjectItem;
    }

    public  WeaponTypeRef createWeaponTypeRef(String typeId) {
        WeaponTypeRef weaponTypeRef = new WeaponTypeRef();
        weaponTypeRef.setOID(UUID.fromString(typeId).toString());
        return weaponTypeRef;
    }

    public  VehicleTypeRef createvehicleTypeRef(String typeId) {
        VehicleTypeRef vehicleTypeRef = new VehicleTypeRef();
        vehicleTypeRef.setOID(UUID.fromString(typeId).toString());
        return vehicleTypeRef;
    }

    public  ObjectItemLocationInObjectItem createObjectItemLocation(double bearing) {
        ObjectItemLocation itemLocation = new ObjectItemLocation();
        itemLocation.setOID(generateUUID()); // Auto-generated
        itemLocation.setCreatorId(me.getOID());
        if (Double.isFinite(bearing)) {
            itemLocation.setBearingAngle(roundDouble(bearing));
        }

        ObjectItemLocationInObjectItem locationInObjectItem = new ObjectItemLocationInObjectItem();
        locationInObjectItem.setObjectItemLocation(itemLocation);
        return locationInObjectItem;
    }

    public  GeographicPoint createGeographicPoint(double lat, double lon, double alt) {
        VerticalDistance verticalDistance = createVerticalDistance(alt);
        GeographicPoint geographicPoint = new GeographicPoint();
        geographicPoint.setOID(generateUUID()); // Auto-generated
        geographicPoint.setCreatorId(me.getOID());
        geographicPoint.setLatitudeCoordinate(roundDouble(lat));
        geographicPoint.setLongitudeCoordinate(roundDouble(lon));
        geographicPoint.setVerticalDistance(verticalDistance);
        return geographicPoint;
    }

    public  VerticalDistance createVerticalDistance(double alt) {
        if (! Double.isFinite(alt)) {
            return null;
        }
        // Vretical pos
        VerticalDistance verticalDistance = new VerticalDistance();
        verticalDistance.setOID(generateUUID());
        verticalDistance.setCreatorId(me.getOID());
        verticalDistance.setReferenceCode(VerticalDistanceReferenceCode.TOPOSR);
        verticalDistance.setDimension(roundDouble(alt));
        return verticalDistance;
    }

    private  BigDecimal roundDouble(double val) {
        return new BigDecimal(val).setScale(6, BigDecimal.ROUND_CEILING);
    }

    private String generateUUID() {
        return UUID.randomUUID().toString();
    }
}
