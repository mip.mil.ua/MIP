package ua.mil;

import ua.mil.jc3iedm.*;

import java.util.Date;

/**
 * Created by alex on 08.12.15.
 */
public class Test4 {
    public static final String comment = "Вражеский ПТРК(lat = 15.0, lon = 15.0) и Вражеский БТР(lat = 14.9, lon = 14.9)";

    private static OtherMateriel createObject1(MipHelper mipHelper, ObjectItemHostilityStatusRef hostilityStatusRef, OtherReportingData reportingData) {

        OtherReportingDataRef reportingDataRef = new OtherReportingDataRef();
        reportingDataRef.setOID(reportingData.getOID());

        final double lat = 15.0;
        final double lon = 15.0;
        final double alt = Double.NaN;
        final double bearing = Double.NaN;
        final String typeId = "6618a0b3-f473-4f67-843c-4d2030b66dfd";
        final String objectName = "Вражеский ПТРК";

        return createObject(mipHelper, hostilityStatusRef, reportingDataRef, lat, lon, alt, bearing, mipHelper.createWeaponTypeRef(typeId), objectName);
    }

    private static OtherMateriel createObject2(MipHelper mipHelper, ObjectItemHostilityStatusRef hostilityStatusRef, OtherReportingData reportingData) {

        OtherReportingDataRef reportingDataRef = new OtherReportingDataRef();
        reportingDataRef.setOID(reportingData.getOID());

        final double lat = 14.9;
        final double lon = 14.9;
        final double alt = Double.NaN;
        final double bearing = Double.NaN;
        final String typeId = "69a8fe95-9977-46f7-a7de-6add479a177f";
        final String objectName = "Вражеский БТР";

        return createObject(mipHelper, hostilityStatusRef, reportingDataRef, lat, lon, alt, bearing, mipHelper.createvehicleTypeRef(typeId), objectName);
    }

    public static JC3IEDM generate(AbstractOrganisationRef me) {
        // ReportingData

        MipHelper mipHelper = new MipHelper(me);

        OtherReportingData reportingData = mipHelper.getOtherReportingData(new Date());
        OtherReportingDataRef reportingDataRef = new OtherReportingDataRef();
        reportingDataRef.setOID(reportingData.getOID());

        ObjectItemHostilityStatus hostilityStatus = mipHelper.createObjectItemHostilityStatus(ObjectItemHostilityStatusCode.HO);
        hostilityStatus.setReportingDataRef(reportingDataRef);
        ObjectItemHostilityStatusRef hostilityStatusRef = new ObjectItemHostilityStatusRef();
        hostilityStatusRef.setOID(hostilityStatus.getOID());

        //-----------------

        OtherMateriel objectItem1 = createObject1(mipHelper, hostilityStatusRef, reportingData);
        OtherMateriel objectItem2 = createObject2(mipHelper, hostilityStatusRef, reportingData);

        JC3IEDM jc3IEDM = new JC3IEDM();
        jc3IEDM.getGeographicPointOrObjectItemCommentOrObjectItemHostilityStatus().add(objectItem1);
        jc3IEDM.getGeographicPointOrObjectItemCommentOrObjectItemHostilityStatus().add(objectItem2);
        jc3IEDM.getGeographicPointOrObjectItemCommentOrObjectItemHostilityStatus().add(reportingData);
        jc3IEDM.getGeographicPointOrObjectItemCommentOrObjectItemHostilityStatus().add(hostilityStatus);

        return jc3IEDM;
    }

    private static OtherMateriel createObject(MipHelper mipHelper, ObjectItemHostilityStatusRef hostilityStatusRef, OtherReportingDataRef reportingDataRef, double lat, double lon, double alt, double bearing, AbstractMaterielTypeRef materielTypeRef, String objectName) {
        // OtherMateriel
        OtherMateriel objectItem = mipHelper.createOtherMateriel(objectName);

        // ObjectItemLocation
        GeographicPoint geographicPoint = mipHelper.createGeographicPoint(lat, lon, alt);
        ObjectItemLocationInObjectItem locationInObjectItem = mipHelper.createObjectItemLocation(bearing);
        locationInObjectItem.setLocation(geographicPoint);
        locationInObjectItem.getObjectItemLocation().setReportingDataRef(reportingDataRef);
        objectItem.getObjectItemLocationInObjectItem().add(locationInObjectItem);
        //-----------------

        // ObjectItemType
        ObjectItemTypeInObjectItem itemTypeInObjectItem = mipHelper.createObjectItemTypeInObjectItemWeapon(materielTypeRef);
        itemTypeInObjectItem.getObjectItemType().setReportingDataRef(reportingDataRef);
        objectItem.getObjectItemTypeInObjectItem().add(itemTypeInObjectItem);
        //-----------------

        //Hostility
        objectItem.getHostilityStatusOrHostilityStatusRef().add(hostilityStatusRef);
        return objectItem;
    }
}
