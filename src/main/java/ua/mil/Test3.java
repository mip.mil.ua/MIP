package ua.mil;

import ua.mil.jc3iedm.*;

import java.util.Date;

/**
 * Created by alex on 08.12.15.
 */
public class Test3 {
    public static final String comment = "\"ПТРК\": lat = 15.0, lon = 15.0, alt = Nan, два отчета (тип и позиция)";

    private static OtherMateriel createObject(MipHelper mipHelper, OtherReportingData reportingData1, OtherReportingData reportingData2) {

        final double lat = 15.0;
        final double lon = 15.0;
        final double alt = Double.NaN;
        final double bearing = Double.NaN;
        final String objectName = "ПТРК";
        final String typeId = "6618a0b3-f473-4f67-843c-4d2030b66dfd";

        // OtherMateriel
        OtherMateriel objectItem = mipHelper.createOtherMateriel(objectName);

        // ObjectItemLocation
        GeographicPoint geographicPoint = mipHelper.createGeographicPoint(lat, lon, alt);
        ObjectItemLocationInObjectItem locationInObjectItem = mipHelper.createObjectItemLocation(bearing);
        locationInObjectItem.setLocation(geographicPoint);
        locationInObjectItem.getObjectItemLocation().setReportingData(reportingData1);
        objectItem.getObjectItemLocationInObjectItem().add(locationInObjectItem);
        //-----------------

        // ObjectItemType
        ObjectItemTypeInObjectItem itemTypeInObjectItem = mipHelper.createObjectItemTypeInObjectItemWeapon(mipHelper.createWeaponTypeRef(typeId));
        itemTypeInObjectItem.getObjectItemType().setReportingData(reportingData2);
        objectItem.getObjectItemTypeInObjectItem().add(itemTypeInObjectItem);
        //-----------------

        return objectItem;
    }

    public static JC3IEDM generate(AbstractOrganisationRef me) {
        // ReportingData

        MipHelper mipHelper = new MipHelper(me);

        Date date1 = new Date();
        OtherReportingData reportingData1 = mipHelper.getOtherReportingData(date1);
        Date date2 = new Date(date1.getTime() - 3645 * 1000);
        OtherReportingData reportingData2 = mipHelper.getOtherReportingData(date2);
        //-----------------

        OtherMateriel objectItem = createObject(mipHelper, reportingData1, reportingData2);
        JC3IEDM jc3IEDM = new JC3IEDM();
        jc3IEDM.getGeographicPointOrObjectItemCommentOrObjectItemHostilityStatus().add(objectItem);

        return jc3IEDM;
    }
}
