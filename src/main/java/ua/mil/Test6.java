package ua.mil;

import ua.mil.jc3iedm.*;

import java.util.Date;

/**
 * Created by alex on 08.12.15.
 */
public class Test6 {
    public static final String comment = "ПТРК и БТР с общией локацией (lat = 15.0, lon = 15.0, alt = 50)";

    private static OtherMateriel createObject1(MipHelper mipHelper, GeographicPointRef locationRef, OtherReportingData reportingData) {

        OtherReportingDataRef reportingDataRef = new OtherReportingDataRef();
        reportingDataRef.setOID(reportingData.getOID());

        final double bearing = 100;
        final String typeId = "6618a0b3-f473-4f67-843c-4d2030b66dfd";
        final String objectName = "ПТРК";

        return createObject(mipHelper, locationRef, reportingDataRef, bearing, mipHelper.createWeaponTypeRef(typeId), objectName);
    }

    private static OtherMateriel createObject2(MipHelper mipHelper, GeographicPointRef locationRef, OtherReportingData reportingData) {

        OtherReportingDataRef reportingDataRef = new OtherReportingDataRef();
        reportingDataRef.setOID(reportingData.getOID());

        final double bearing = 10;
        final String typeId = "69a8fe95-9977-46f7-a7de-6add479a177f";
        final String objectName = "БТР";

        return createObject(mipHelper, locationRef, reportingDataRef, bearing, mipHelper.createvehicleTypeRef(typeId), objectName);
    }

    public static JC3IEDM generate(AbstractOrganisationRef me) {
        // ReportingData

        MipHelper mipHelper = new MipHelper(me);

        OtherReportingData reportingData = mipHelper.getOtherReportingData(new Date());
        OtherReportingDataRef reportingDataRef = new OtherReportingDataRef();
        reportingDataRef.setOID(reportingData.getOID());

        // Location
        final double lat = 15.0;
        final double lon = 15.0;
        final double alt = 50;
        GeographicPoint locaton = mipHelper.createGeographicPoint(lat, lon, alt);
        GeographicPointRef geographicPointRef = new GeographicPointRef();
        geographicPointRef.setOID(locaton.getOID());

        //-----------------

        OtherMateriel objectItem1 = createObject1(mipHelper, geographicPointRef, reportingData);
        OtherMateriel objectItem2 = createObject2(mipHelper, geographicPointRef, reportingData);

        JC3IEDM jc3IEDM = new JC3IEDM();
        jc3IEDM.getGeographicPointOrObjectItemCommentOrObjectItemHostilityStatus().add(objectItem1);
        jc3IEDM.getGeographicPointOrObjectItemCommentOrObjectItemHostilityStatus().add(objectItem2);
        jc3IEDM.getGeographicPointOrObjectItemCommentOrObjectItemHostilityStatus().add(reportingData);
        jc3IEDM.getGeographicPointOrObjectItemCommentOrObjectItemHostilityStatus().add(locaton);

        return jc3IEDM;
    }

    private static OtherMateriel createObject(MipHelper mipHelper, GeographicPointRef locationRef, OtherReportingDataRef reportingDataRef, double bearing, AbstractMaterielTypeRef materielTypeRef, String objectName) {
        // OtherMateriel
        OtherMateriel objectItem = mipHelper.createOtherMateriel(objectName);

        // ObjectItemLocation
        ObjectItemLocationInObjectItem locationInObjectItem = mipHelper.createObjectItemLocation(bearing);
        locationInObjectItem.setLocationRef(locationRef);
        locationInObjectItem.getObjectItemLocation().setReportingDataRef(reportingDataRef);
        objectItem.getObjectItemLocationInObjectItem().add(locationInObjectItem);
        //-----------------

        // ObjectItemType
        ObjectItemTypeInObjectItem itemTypeInObjectItem = mipHelper.createObjectItemTypeInObjectItemWeapon(materielTypeRef);
        itemTypeInObjectItem.getObjectItemType().setReportingDataRef(reportingDataRef);
        objectItem.getObjectItemTypeInObjectItem().add(itemTypeInObjectItem);
        //-----------------

        return objectItem;
    }
}
